classdef test_FilterCascade < matlab.unittest.TestCase
  
  methods (Test)
    function testConstructor_ArgumentsMustBeLessThanTwo(testCase)
      % test that 0 arguments passes
      testCase.assertNotEmpty(FilterCascade);
      
      % test that 1 argument passes
      testCase.assertNotEmpty(FilterCascade(1:3));
      
      % test that 2+ arguments fails
      testCase.verifyError(@()FilterCascade(1:3, 'test'), ...
                           'MATLAB:TooManyInputs');
    end
    
    function testConstructor_ArgumentMustBeNumericArray(testCase)
      % test that single number fails
      testCase.verifyError(@()FilterCascade(1), ...
                           'FilterCascade:WrongInputTypeToConstructor');
                         
      % test that string fails
      testCase.verifyError(@()FilterCascade('test'), ...
                           'FilterCascade:WrongInputTypeToConstructor');
                         
      % test that cell fails
      testCase.verifyError(@()FilterCascade({1}), ...
                           'FilterCascade:WrongInputTypeToConstructor');
      testCase.verifyError(@()FilterCascade({'test'}), ...
                           'FilterCascade:WrongInputTypeToConstructor');
    end
    
    function testConstructor_DefaultComponentValuesPresent(testCase)
      test = FilterCascade();
      
      % test that the sampling frequency is present
      testCase.assertEqual(test.fs, 250);
      
      % test that the gain is present
      testCase.assertEqual(get(test.gain, 'numerator'), 100);
      
      % test that the resistors are present
      testCase.assertEqual(test.R34, 10e6);
      testCase.assertEqual(test.R35, 10e6);
      testCase.assertEqual(test.R37, 10e6);
      testCase.assertEqual(test.R42, 20000e3);
      testCase.assertEqual(test.R43, 10e6);
      testCase.assertEqual(test.R46, 10e6);
      testCase.assertEqual(test.R48, 750e3);
      testCase.assertEqual(test.R49, 10e6);
      testCase.assertEqual(test.R47, 750e3);
      testCase.assertEqual(test.R54, 2.49e3);
      testCase.assertEqual(test.R55, 750e3);
      testCase.assertEqual(test.R52, 1500e3);
      
      % test that the capacitors are present
      testCase.assertEqual(test.C34, 10e-9);
      testCase.assertEqual(test.C35, 22e-6);
      testCase.assertEqual(test.C39, 4.7e-9);
      testCase.assertEqual(test.C40, 4.7e-9);
    end
    
    function testMultiplyByGain_testMultiplyByGainCorrect(testCase)
      % test that multiplying by the gain gives the correct values
      testCase.assertEqual(FilterCascade([1, 1, 1]).multiplyByGain().s, ...
                           [100, 100, 100]);
    end
    
    function test_ApplyFilter1(testCase)
      
      % test DC offset removal
      n = 5e6;
      f0 = 4; % Hz
      fs = 250; % Hz
      theta0 = 2 * pi * f0 / fs;
      testSignal = cos(theta0 * (0:(n - 1)));

      test = FilterCascade((1 + testSignal));
      s = test.applyFilter1();
      
      startSegment = n - 10*fs;
      endSegment   = n;
      
      actualSegment = s(startSegment:endSegment);
      expectSegment = testSignal(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, 1);
      
      % test baseline wander removal
      f1 = 0.00001; % Hz
      theta1 = 2 * pi * f1 / fs;
      baselineWander = cos(theta1 * (0:(n - 1)));
      
      fullTestSignal = baselineWander + testSignal;
      
      maxTestableSigAmp = 0.1 * abs(max(fullTestSignal) - min(fullTestSignal));
      
      test = FilterCascade(fullTestSignal);
      s = test.applyFilter1();
      
      % check that remaining signal after filtering is the original test signal
      actualSegment = s(startSegment:endSegment);
      expectSegment = testSignal(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp);
              
      % check that the signal portion filtered out is the baseline wander signal
      sBaselineWander = fullTestSignal - s;
      
      actualSegment = sBaselineWander(startSegment:endSegment);
      expectSegment = baselineWander(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp);
      
    end
    
    function test_ApplyFilter2(testCase)
      
      % test high frequency noise removal
      n = 5e6;
      f0 = 4; % Hz
      f1 = 90; % Hz
      fs = 250; % Hz
      theta0 = 2 * pi * f0 / fs;
      theta1 = 2 * pi * f1 / fs;
      testSignal = cos(theta0 * (0:(n - 1)));
      noise = 0.5 * cos(theta1 * (0:(n - 1)));
      
      fullTestSignal = testSignal + noise;
      
      maxTestableSigAmp = 0.1 * abs(max(fullTestSignal) - min(fullTestSignal));

      test = FilterCascade(fullTestSignal);
      [s, test] = test.applyFilter2();
      
      % check that remaining signal after filtering is the original test signal
      startSegment = n - 10*fs;
      endSegment   = n;
      
      actualSegment = s(startSegment:endSegment) / 1.5;
      expectSegment = testSignal(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp * 1.5);
      
      % check that the signal portion filtered out is the noise signal
      sNoise = fullTestSignal - (s / 1.5);
      
      actualSegment = sNoise(startSegment:endSegment) / 1.5;
      expectSegment = noise(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp * 1.5);
    end
    
    function test_ApplyFilterBank(testCase)
      
      % test low frequency noise removal
      n = 5e6;
      f0 = 4; % Hz
      f1 = 0.1; % Hz
      fs = 250; % Hz
      theta0 = 2 * pi * f0 / fs;
      theta1 = 2 * pi * f1 / fs;
      testSignal = cos(theta0 * (0:(n - 1)));
      baselineWander = 0.5 * cos(theta1 * (0:(n - 1)));
      
      fullTestSignal = testSignal + baselineWander;
      
      maxTestableSigAmp = 0.1 * abs(max(fullTestSignal) - min(fullTestSignal));

      test = FilterCascade(fullTestSignal);
      [sHigh, sLow, test] = test.applyFilterBank();
      
      % check that remaining signal after filtering is the original test signal
      startSegment = n - 10*fs;
      endSegment   = n;
      
      actualSegment = sHigh(startSegment:endSegment) / 2;
      expectSegment = testSignal(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp * 2);
      
      % check that the signal portion filtered out is the baseline wander signal
%       sBaselineWander = fullTestSignal - (sHigh / 2);
      
      actualSegment = sLow(startSegment:endSegment);
      expectSegment = baselineWander(startSegment:endSegment);
      
      filterDifference = abs(actualSegment - expectSegment);
      testCase.assertLessThan(filterDifference, maxTestableSigAmp * 2);
      
    end
    
    function test_plotMagResponse(testCase)
      % test that a non-valid filter combination fails
      test = FilterCascade([1, 1, 1]);
      testCase.verifyError(@()test.plotResponse(5), ...
                           'FilterCascade:InputNotCharArray');
      
      % test that a non-valid filter combination fails
      test = FilterCascade([1, 1, 1]);
      testCase.verifyError(@()test.plotResponse('56789'), ...
                           'FilterCascade:InputNotValid');
      
    end
  end
  
end