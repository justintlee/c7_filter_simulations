close all;
clear;
clc;

% script parameters
mpath  = 'C:\Users\justin.lee\Documents\datasets\VT FP\325971315\325971315';
heaext = '.hea'; % header - file extension
datext = '.dat'; % data - file extension

channel = 1;

udtfile = strcat(mpath, datext);
uhdfile = strcat(mpath, heaext);

%--------------------
%Get file information
%--------------------
fileinfo = dir(udtfile);
sizeoffile = fileinfo.bytes;
uheader.nsamp = sizeoffile / 3;

%------------------
%Read ECG data file
%------------------
udata = rdsign212(udtfile, 2, 0, uheader.nsamp - 1);
[recname, uheader] = readheader(uhdfile);

s = udata(:, channel) / uheader.gain(channel);

%-----------------
%Perform filtering
%-----------------
fc = FilterCascade(s);
fc = fc.multiplyByGain();
[s1, fc] = fc.applyFilter1();
[s2, fc] = fc.applyFilter2();
[sHigh, sLow, fc] = fc.applyFilterBank();

sRecon = 0.5 * sHigh + sLow;

SSE = sum((s - sRecon).^2);
MSE = SSE / uheader.nsamp;
disp(MSE);

%------------
%Plot results
%------------
figure();
a1 = subplot(3, 1, 1);
plot(s, 'k');
hold on;
plot(sRecon / 100 / 1.5, 'r--');
hold off;

a2 = subplot(3, 1, 2);
plot(s1 / 100, 'k');
hold on;
plot(s2 / 100 / 1.5, 'r--');
hold off;

a3 = subplot(3, 1, 3);
plot(sHigh / 100 / 2, 'r--');
hold on;
plot(sLow / 100, 'b--');
hold off;

linkaxes([a1 a2 a3], 'x');