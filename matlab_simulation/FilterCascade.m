classdef FilterCascade < handle
  properties
    s
    sHigh
    sLow
    
    % sampling frequency value
    fs = 250; % Hz
    
    % gain value
    gain = dfilt.df1(100, 1);
    
    % resistor values
    R34 = 10e6;    % Ohms
    R35 = 10e6;    % Ohms
    R37 = 10e6;    % Ohms
%     R42 = 10000e3; % Ohms
    R42 = 20000e3; % Ohms
    R43 = 10e6;    % Ohms
    R46 = 10e6;    % Ohms
    R47 = 750e3;   % Ohms
    R48 = 750e3;   % Ohms
    R49 = 10e6;    % Ohms
    R54 = 2.49e3;  % Ohms
    R55 = 750e3;   % Ohms
    R52 = 1500e3;  % Ohms
    
    % capacitor values
    C34 = 10e-9;   % F
%     C35 = 4700e-9; % F
    C35 = 22e-6;   % F
    C39 = 4.7e-9;  % F
    C40 = 4.7e-9;  % F
    
    % analog filter structures
    analogFilter1 = struct();
    analogFilter2 = struct();
    analogFilter3 = struct();
    analogFilter4 = struct();
    
    % digital filter structures
    digitalFilter1 = dfilt.df1;
    digitalFilter2 = dfilt.df1;
    digitalFilter3 = dfilt.df1;
    digitalFilter4 = dfilt.df1;
  end
  
  methods
    % constructor method
    function obj = FilterCascade(s)
      
      % ensure that input is of the correct form
      if nargin == 0
        obj.s = [];
      elseif nargin == 1
        if isvector(s) && isnumeric(s) && ~isscalar(s)
          obj.s = s;
        else
          error('FilterCascade:WrongInputTypeToConstructor', ...
                'Input must be a numeric array');
        end
      else
      end
      
      % calculate filter coefficients for all stages
      obj = obj.initializeFilters();
      
    end
    
    % method for initializing filters
    function obj = initializeFilters(obj)
      % frequency sweeps
      analogAngFreq = 2 * pi * logspace(-4, 3, 5e6);  % rad / s
      digitalAngFreq  = analogAngFreq / (obj.fs / 2); % rad / sample
      
      % FILTER #1 - HPF (-3 dB cutoff at 0.34 Hz)
      
      % create transfer function
      obj.analogFilter1.b = [obj.R42 * obj.C35 / 100, 0];
      obj.analogFilter1.a = [obj.R42 * obj.C35 / 100, 1];
      
      % determine analog frequency response
      analogFreqResp1 = freqs(obj.analogFilter1.b, ...
                              obj.analogFilter1.a, ...
                              analogAngFreq);          % complex freq. resp.
      analogFreqMag1  = abs(analogFreqResp1);          % magnitude
      
      % ensure that analog filter meets nominal cutoff frequency of 0.34 Hz
      freqCutoff             = 1 / (2 * pi * obj.R42 * obj.C35 / 100); % Hz
      analogAngFreqCutoffVal = 2 * pi * freqCutoff;                    % rad / s
      cutoffMag              = 1 / sqrt(2);                            % mag.
      
      analogAngFreqCutoffInd  = abs(analogFreqMag1 - cutoffMag) < 1e-2;
      analogAngFreqCutoffEst  = analogAngFreq(analogAngFreqCutoffInd); % rad / s
      analogAngFreqCutoffDiff = abs(analogAngFreqCutoffEst ...
                                    - analogAngFreqCutoffVal);         % rad / s

      assert(sum(analogAngFreqCutoffDiff < 1e-3) > 0, ...
             'FilterCascade:AnalogFilterNotMeetCutoff', ...
             'The analog filter does not meet the nominal cutoff frequency');

      % transform to digital filter
      [b_digital, a_digital] = bilinear(obj.analogFilter1.b, ...
                                        obj.analogFilter1.a, ...
                                        obj.fs);
      obj.digitalFilter1 = dfilt.df1(b_digital, a_digital);
      
      % ensure that digital filter meets nominal cutoff frequency of 0.34 Hz
      digitalFreqResp1 = obj.digitalFilter1.freqz(digitalAngFreq); % complex
                                                                   % freq. resp.
      digitalFreqMag1  = abs(digitalFreqResp1);                    % magnitude
      
      digitalAngFreqCutoffVal  = analogAngFreqCutoffVal / obj.fs; % rad / s
      digitalAngFreqCutoffInd  = abs(digitalFreqMag1 ...
                                     - cutoffMag) < 1e-2;
      digitalAngFreqCutoffEst  = digitalAngFreq(...
                                   digitalAngFreqCutoffInd);      % rad / s
      digitalAngFreqCutoffDiff = abs(digitalAngFreqCutoffEst ...
                                    - digitalAngFreqCutoffVal);   % rad / s
      
      assert(sum(digitalAngFreqCutoffDiff < 1e-5) > 0, ...
             'FilterCascade:DigitalFilterNotMeetCutoff', ...
             'The digital filter does not meet the nominal cutoff frequency');
      
      % FILTER #2 - LPF (cutoff at 45.15 Hz)
           
      % create gain (K = 1 + R4/R3)
      K2 = 1 + obj.R55 / obj.R52;
      
      % ensure that gain meets Sallen-Key specifications
      assert(K2 == 1.5, ...
             'FilterCascade:GainDoesNotMeetSpec', ...
             'The digital filter does not have the correct gain');
           
      % create quality factor Q
      %           sqrt(R1R2C1C2)
      % Q = -------------------------
      %     R1C1 + R2C1 + R1C2(1 - K)
      %       R1        R2        C1        C2
      D21 = obj.R48 * obj.R47 * obj.C39 * obj.C40;
      D22 = obj.R48 * obj.C39 ...
            + obj.R47 * obj.C39 ...
            + obj.R48 * obj.C40 * (1 - K2);
      Q2  = sqrt(D21) / D22;
      
      % ensure that quality factor meets Sallen-Key specifications
      assert((Q2 - 2/3) < 1e-3, ...
             'FilterCascade:QualityFactorDoesNotMeetSpec', ...
             'The digital filter does not have the correct Q value');
           
      % create transfer function
      obj.analogFilter2.b = K2;
      obj.analogFilter2.a = [D21, D22, 1];
      
      % determine analog frequency response
      analogFreqResp2 = freqs(obj.analogFilter2.b, ...
                              obj.analogFilter2.a, ...
                              analogAngFreq);          % complex freq. resp.
      analogFreqMag2  = abs(analogFreqResp2);          % magnitude
               
      % ensure that analog filter meets nominal cutoff frequency of 45.15 Hz
      freqCutoff             = 1/(2 * pi * sqrt(D21)); % Hz
      analogAngFreqCutoffVal = 2 * pi * freqCutoff;    % rad / s
      cutoffMag              = K2 * Q2;                % magnitude

      analogAngFreqCutoffInd  = abs(analogFreqMag2 - cutoffMag) < 1e-2;
      analogAngFreqCutoffEst  = analogAngFreq(analogAngFreqCutoffInd); % rad / s
      analogAngFreqCutoffDiff = abs(analogAngFreqCutoffEst ...
                                    - analogAngFreqCutoffVal);         % rad / s
      
      assert((sum(analogAngFreqCutoffDiff < 1e-2)) > 0, ...
             'FilterCascade:AnalogFilterNotMeetCutoff', ...
             'The analog filter does not meet the nominal cutoff frequency');
           
      % transform to digital filter
      [b_digital, a_digital] = bilinear(obj.analogFilter2.b, ...
                                        obj.analogFilter2.a, ...
                                        obj.fs, ...
                                        freqCutoff);
      obj.digitalFilter2 = dfilt.df1(b_digital, a_digital);
      
      % ensure that digital filter meets nominal cutoff frequency of 45.15 Hz
      digitalAngFreq   = analogAngFreq / (obj.fs / 2);             % rad / s
      digitalFreqResp2 = obj.digitalFilter2.freqz(digitalAngFreq); % complex
                                                                   % freq. resp.
      digitalFreqMag2  = abs(digitalFreqResp2);                    % magnitude
      
      digitalAngFreqCutoffVal  = analogAngFreqCutoffVal / obj.fs; % rad / s
      digitalAngFreqCutoffInd  = abs(digitalFreqMag2 ...
                                   - cutoffMag) < 1e-2;
      digitalAngFreqCutoffEst  = digitalAngFreq(...
                                   digitalAngFreqCutoffInd);      % rad / s
      digitalAngFreqCutoffDiff = abs(digitalAngFreqCutoffEst ...
                                   - digitalAngFreqCutoffVal);    % rad / s
      
      assert(sum(digitalAngFreqCutoffDiff < 1e-1) > 0, ...
             'FilterCascade:DigitalFilterNotMeetCutoff', ...
             'The digital filter does not meet the nominal cutoff frequency');
           
      % FILTER #3 - HPF (cutoff at 1.6 Hz)
      
      % create gain (K = 1 + R4/R3)
      K3 = 1 + obj.R37 / obj.R35;
      
      % ensure that gain meets Sallen-Key specifications
      assert(K3 == 2, ...
             'FilterCascade:GainDoesNotMeetSpec', ...
             'The digital filter does not have the correct gain');
      
      % create transfer function
      obj.analogFilter3.b = [K3 * obj.R34 * obj.C34, 0];
      obj.analogFilter3.a = [obj.R34 * obj.C34, 1];

      % determine analog frequency response
      analogFreqResp3 = freqs(obj.analogFilter3.b, ...
                              obj.analogFilter3.a, ...
                              analogAngFreq);          % complex freq. resp.
      analogFreqMag3  = abs(analogFreqResp3);          % magnitude
               
      % ensure that analog filter meets nominal cutoff frequency of 1.6 Hz
      freqCutoff             = 1/(2 * pi * obj.R34 * obj.C34); % Hz
      analogAngFreqCutoffVal = 2 * pi * freqCutoff;            % rad / s
      cutoffMag              = K3 / sqrt(2);                   % magnitude

      analogAngFreqCutoffInd  = abs(analogFreqMag3 - cutoffMag) < 1e-2;
      analogAngFreqCutoffEst  = analogAngFreq(analogAngFreqCutoffInd); % rad / s
      analogAngFreqCutoffDiff = abs(analogAngFreqCutoffEst ...
                                    - analogAngFreqCutoffVal);         % rad / s
      
      assert((sum(analogAngFreqCutoffDiff < 1e-2)) > 0, ...
             'FilterCascade:AnalogFilterNotMeetCutoff', ...
             'The analog filter does not meet the nominal cutoff frequency');
           
      % transform to digital filter
      [b_digital, a_digital] = bilinear(obj.analogFilter3.b, ...
                                        obj.analogFilter3.a, ...
                                        obj.fs, ...
                                        freqCutoff);
      obj.digitalFilter3 = dfilt.df1(b_digital, a_digital);
      
      % ensure that digital filter meets nominal cutoff frequency of 1.6 Hz
      digitalAngFreq   = analogAngFreq / (obj.fs / 2);             % rad / s
      digitalFreqResp3 = obj.digitalFilter3.freqz(digitalAngFreq); % complex
                                                                   % freq. resp.
      digitalFreqMag3  = abs(digitalFreqResp3);                    % magnitude
      
      digitalAngFreqCutoffVal  = analogAngFreqCutoffVal / obj.fs; % rad / s
      digitalAngFreqCutoffInd  = abs(digitalFreqMag3 ...
                                   - cutoffMag) < 1e-1;
      digitalAngFreqCutoffEst  = digitalAngFreq(...
                                   digitalAngFreqCutoffInd);      % rad / s
      digitalAngFreqCutoffDiff = abs(digitalAngFreqCutoffEst ...
                                   - digitalAngFreqCutoffVal);    % rad / s
      
      assert(sum(digitalAngFreqCutoffDiff < 1e-1) > 0, ...
             'FilterCascade:DigitalFilterNotMeetCutoff', ...
             'The digital filter does not meet the nominal cutoff frequency');
           
      % FILTER #4 - LPF (cutoff at 1.6 Hz)
      % cascade with previous stages
      cascadedAnalogFilter = struct();
      cascadedAnalogFilter.b = 1;
      cascadedAnalogFilter.a = 1;
      
      cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                    obj.analogFilter1.b);
      cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                    obj.analogFilter1.a);
      cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                    obj.analogFilter2.b);
      cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                    obj.analogFilter2.a);
      wideband = cascadedAnalogFilter;
      cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                    obj.analogFilter3.b);
      cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                    obj.analogFilter3.a);
      highband = cascadedAnalogFilter;
      
      obj.analogFilter4.b = 2 * conv(highband.a, wideband.b) ...
                            - conv(wideband.a, highband.b);
      obj.analogFilter4.a = 4 * conv(wideband.a, highband.a);
      
      % determine analog frequency response
      analogFreqResp4 = freqs(obj.analogFilter4.b, ...
                              obj.analogFilter4.a, ...
                              analogAngFreq);          % complex freq. resp.
      analogFreqMag4  = abs(analogFreqResp4);          % magnitude
      
      % scale so that the transfer function produces a magnitude response
      % that is half of the wideband when cascaded
      scalingConstant = 0.50 / max(analogFreqMag4);
      obj.analogFilter4.b = obj.analogFilter4.b * scalingConstant;
               
      % ensure that analog filter meets nominal cutoff frequency of 1.6 Hz
      freqCutoff             = 1.6;                            % Hz
      analogAngFreqCutoffVal = 2 * pi * freqCutoff;            % rad / s
      cutoffMag              = max(analogFreqMag4) / sqrt(2);  % magnitude

      analogAngFreqCutoffInd  = abs(analogFreqMag4 - cutoffMag) < 5e-1;
      analogAngFreqCutoffEst  = analogAngFreq(analogAngFreqCutoffInd); % rad / s
      analogAngFreqCutoffDiff = abs(analogAngFreqCutoffEst ...
                                    - analogAngFreqCutoffVal);         % rad / s
      
      assert((sum(analogAngFreqCutoffDiff < 5e-1)) > 0, ...
             'FilterCascade:AnalogFilterNotMeetCutoff', ...
             'The analog filter does not meet the nominal cutoff frequency');
           
      % transform to digital filter
      [b_digital, a_digital] = bilinear(obj.analogFilter4.b, ...
                                        obj.analogFilter4.a, ...
                                        obj.fs, ...
                                        freqCutoff);
      obj.digitalFilter4 = dfilt.df1(b_digital, a_digital);
      
      % ensure that digital filter meets nominal cutoff frequency of 1.6 Hz
      digitalAngFreq   = analogAngFreq / (obj.fs / 2);             % rad / s
      digitalFreqResp4 = obj.digitalFilter4.freqz(digitalAngFreq); % complex
                                                                   % freq. resp.
      digitalFreqMag4  = abs(digitalFreqResp4);                    % magnitude
      
      digitalAngFreqCutoffVal  = analogAngFreqCutoffVal / obj.fs; % rad / s
      digitalAngFreqCutoffInd  = abs(digitalFreqMag4 ...
                                   - cutoffMag) < 1e-1;
      digitalAngFreqCutoffEst  = digitalAngFreq(...
                                   digitalAngFreqCutoffInd);      % rad / s
      digitalAngFreqCutoffDiff = abs(digitalAngFreqCutoffEst ...
                                   - digitalAngFreqCutoffVal);    % rad / s
                                 
      assert(sum(digitalAngFreqCutoffDiff < 1e-1) > 0, ...
             'FilterCascade:DigitalFilterNotMeetCutoff', ...
             'The digital filter does not meet the nominal cutoff frequency');
      
    end
    
    % method for multiplying input signal by gain
    function obj = multiplyByGain(obj)
      obj.s = obj.gain.filter(obj.s);
    end
    
    % method for applying filter stage #1 to the signal
    % HPF (-3 dB cutoff at 0.34 Hz)
    function [s, obj] = applyFilter1(obj)
      % perform filtering
      s = obj.digitalFilter1.filter(obj.s);
      
      obj.s = s;
    end
    
    % method for applying filter stage #2 to the signal
    % LPF (frequency cutoff at 45.15 Hz)
    function [s, obj] = applyFilter2(obj)
      % perform filtering
      s = obj.digitalFilter2.filter(obj.s);
      
      obj.s = s;
    end
    
    % method for applying filter stage #3 to the signal
    % BPF (frequency cutoffs at 0.34 Hz and 1.6 Hz)
    % HPF (frequency cutoff at 1.6 Hz)
    function [sHigh, sLow, obj] = applyFilterBank(obj)
      % perform filtering
      sHigh = obj.digitalFilter3.filter(obj.s);
%       sLow  = obj.digitalFilter4.filter(obj.s);
      sLow  = 0.5 * (obj.s - 0.5 * sHigh);
      
      obj.sHigh = sHigh;
      obj.sLow  = sLow;
    end
    
    % method for visualizing filter response
    function obj = plotResponse(obj, filterStage)
      
      % check filterStage input
      assert(ischar(filterStage), ...
             'FilterCascade:InputNotCharArray', ...
             'Input to method must be character array');
      
      if ~isempty(regexpi(filterStage, '[56789A-Za-z]'))
          throw(MException('FilterCascade:InputNotValid', ...
                           'Not a valid combination'));
      end
        
      % parameters
      analogAngFreq  = 2 * pi * logspace(-4, 3, 5e6);   % rad / s
      digitalAngFreq = analogAngFreq / obj.fs; % rad / s
      
      K2  = 1 + obj.R55 / obj.R52;
      D21 = obj.R48 * obj.R47 * obj.C39 * obj.C40;
      D22 = obj.R48 * obj.C39 ...
            + obj.R47 * obj.C40 ...
            + obj.R48 * obj.C40 * (1 - K2);
      Q2  = sqrt(D21) / D22;
      
      K3  = 1 + obj.R37 / obj.R35;
      
      % determine filter stages
      cascadedAnalogFilter = struct();
      cascadedAnalogFilter.b = 1;
      cascadedAnalogFilter.a = 1;
      filterList = dfilt.df1(1, 1);
      
      if strfind(filterStage, '0')
        cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                      100);
        cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                      1);
        filterList = [filterList; obj.gain];
      end
      
      if strfind(filterStage, '1')
        cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                      obj.analogFilter1.b);
        cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                      obj.analogFilter1.a);
        filterList = [filterList; obj.digitalFilter1];
      end
      
      if strfind(filterStage, '2')
        cascadedAnalogFilter.b = conv(cascadedAnalogFilter.b, ...
                                      obj.analogFilter2.b);
        cascadedAnalogFilter.a = conv(cascadedAnalogFilter.a, ...
                                      obj.analogFilter2.a);
        filterList = [filterList; obj.digitalFilter2];
      end
      
      % cascade all non-filter bank commponents
      cascadedDigitalFilter = dfilt.cascade(filterList);
      
      % determine analog frequency response
      analogFreqResp = freqs(cascadedAnalogFilter.b, ...
                             cascadedAnalogFilter.a, ...
                             analogAngFreq);          % complex freq. resp.
      analogFreqMag  = abs(analogFreqResp);           % magnitude
      
      % determine digital frequency response
      digitalFreqResp = cascadedDigitalFilter.freqz(digitalAngFreq); % complex
                                                                     % freq.
                                                                     % resp.
      digitalFreqMag  = abs(digitalFreqResp);                        % mag.
      
      % determine filter bank responses
      if ~isempty(strfind(filterStage, '3')) ...
         || ~isempty(strfind(filterStage, '4'))
        % 3rd stage filter - analog
        cascadedAnalogHBFilter   = struct();
        cascadedAnalogHBFilter.b = conv(cascadedAnalogFilter.b, ...
                                        obj.analogFilter3.b);
        cascadedAnalogHBFilter.a = conv(cascadedAnalogFilter.a, ...
                                        obj.analogFilter3.a);
                                    
        % determine 3rd stage frequency response - analog
        [analogFreqRespHighband] = freqs(cascadedAnalogHBFilter.b, ...
                                         cascadedAnalogHBFilter.a, ...
                                         analogAngFreq);
        analogMagRespHighband    = abs(analogFreqRespHighband);
        
        % 4th stage filter - analog
        cascadedAnalogLBFilter   = struct();
        cascadedAnalogLBFilter.b = conv(cascadedAnalogFilter.b, ...
                                        obj.analogFilter4.b);
        cascadedAnalogLBFilter.a = conv(cascadedAnalogFilter.a, ...
                                        obj.analogFilter4.a);
        
        % determine 4th stage frequency response - analog
        [analogFreqRespLowband] = freqs(cascadedAnalogLBFilter.b, ...
                                         cascadedAnalogLBFilter.a, ...
                                         analogAngFreq);
        analogMagRespLowband    = abs(analogFreqRespLowband);
        
        % 3rd stage filter - digital
        cascadedDigitalHBFilter ...
          = cascadedDigitalFilter.cascade(obj.digitalFilter3);
                                    
        % determine 3rd stage frequency response - digital
        [digitalFreqRespHighband] ...
          = cascadedDigitalHBFilter.freqz(digitalAngFreq);
        digitalMagRespHighband    = abs(digitalFreqRespHighband);
        
        % 4th stage filter - digital
        cascadedDigitalLBFilter ...
          = cascadedDigitalFilter.cascade(obj.digitalFilter4);
                                    
        % determine 4th stage frequency response - digital
        [digitalFreqRespLowband] ...
          = cascadedDigitalLBFilter.freqz(digitalAngFreq);
        digitalMagRespLowband    = abs(digitalFreqRespLowband);
      end
      
      % calculate phase delays
      phaseDelayWideband = phasedelay(cascadedDigitalFilter, ...
                                      analogAngFreq / (2 * pi), obj.fs);
      phaseDelayHighband = phasedelay(cascadedDigitalHBFilter, ...
                                      analogAngFreq / (2 * pi), obj.fs);
      phaseDelayLowband  = phasedelay(cascadedDigitalLBFilter, ...
                                      analogAngFreq / (2 * pi), obj.fs);
                                  
      phaseDelayWideband = -unwrap(phaseDelayWideband);
      phaseDelayHighband = -unwrap(phaseDelayHighband);
      phaseDelayLowband  = -unwrap(phaseDelayLowband);
      
      % calculate group delays
      groupDelayWideband = grpdelay(cascadedDigitalFilter, ...
                                    analogAngFreq / (2 * pi), obj.fs);
      groupDelayHighband = grpdelay(cascadedDigitalHBFilter, ...
                                    analogAngFreq / (2 * pi), obj.fs);
      groupDelayLowband  = grpdelay(cascadedDigitalLBFilter, ...
                                    analogAngFreq / (2 * pi), obj.fs);
                                  
      % plot analog filter
      figure();
      subplot(3, 1, 1);
      semilogx(analogAngFreq / (2 * pi), ...
               analogFreqMag, ...
               'k-');
      if ~isempty(strfind(filterStage, '3')) ...
         || ~isempty(strfind(filterStage, '4'))
        hold on;
        semilogx(analogAngFreq / (2 * pi), ...
                 analogMagRespHighband, ...
                 'r--');
        semilogx(analogAngFreq / (2 * pi), ...
                 analogMagRespLowband, ...
                 'b--');
        legend('magnitude - wideband', ...
               'magnitude - high band', ...
               'magnitude - low band', ...
               'Location', 'northwest');
        hold off;
      end
      xlim([10e-4, obj.fs / 2]);
      title('analog filter');
      xlabel('frequency (Hz)');
      ylabel('gain');
      
      % plot digital filter
      subplot(3, 1, 2);
      semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
               digitalFreqMag, ...
               'k-');
      if ~isempty(strfind(filterStage, '3')) ...
         || ~isempty(strfind(filterStage, '4'))
        hold on;
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
                 digitalMagRespHighband, ...
                 'r--');
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
                 digitalMagRespLowband, ...
                 'b--');
        legend('magnitude - wideband', ...
               'magnitude - high band', ...
               'magnitude - low band', ...
               'Location', 'northwest');
        hold off;
      end
      xlim([10e-4, obj.fs / 2]);
      title('digital filter');
      xlabel('frequency (Hz)');
      ylabel('gain');
      
      % plot both
      subplot(3, 1, 3);
      semilogx(analogAngFreq / (2 * pi), ...
               analogFreqMag, ...
               'k-');
      hold on;
      semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
               digitalFreqMag, ...
               'ko', 'MarkerSize', 0.75);
      if ~isempty(strfind(filterStage, '3')) ...
         || ~isempty(strfind(filterStage, '4'))
        semilogx(analogAngFreq / (2 * pi), ...
                 analogMagRespHighband, ...
                 'r-');
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
                 digitalMagRespHighband, ...
                 'ro', 'MarkerSize', 0.75);
        semilogx(analogAngFreq / (2 * pi), ...
                 analogMagRespLowband, ...
                 'b-');
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
                 digitalMagRespLowband, ...
                 'bo', 'MarkerSize', 0.75);
        legend('magnitude - wideband (analog)', ...
               'magnitude - wideband (digital)', ...
               'magnitude - high band (analog)', ...
               'magnitude - high band (digital)', ...
               'magnitude - low band (analog)', ...
               'magnitude - low band (digital)', ...
               'Location', 'northwest');
      else
        legend('magnitude (analog)', ...
               'magnitude (digital)', ...
               'Location', 'northwest');
      end
      hold off;
      xlim([10e-4, obj.fs / 2]);
      title('filters overlaid');
      xlabel('frequency (Hz)');
      ylabel('gain');
      
      % plot group delay
      figure();
      if ~isempty(strfind(filterStage, '3')) ...
         || ~isempty(strfind(filterStage, '4'))
       
        % wideband
        ax1 = subplot(3, 1, 1);
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             groupDelayWideband, ...
             'k-');
        hold on;
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             phaseDelayWideband, ...
             'r--');
        hold off;
        xlim([10e-4, obj.fs / 2]);
        title('wideband group delay');
        ylabel('group delay (samples)');
        legend('group delay', 'phase delay');
        
        % highband
        ax2 = subplot(3, 1, 2);
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             groupDelayHighband, ...
             'k-');
        hold on;
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             phaseDelayHighband, ...
             'r--');
        hold off;
        xlim([10e-4, obj.fs / 2]);
        title('highband group delay');
        ylabel('group delay (samples)');
        legend('group delay', 'phase delay');
        
        % lowband
        ax3 = subplot(3, 1, 3);
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             groupDelayLowband, ...
             'k-');
        hold on;
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             phaseDelayLowband, ...
             'r--');
        hold off;
        xlim([10e-4, obj.fs / 2]);
        title('lowband group delay');
        ylabel('group delay (samples)');
        legend('group delay', 'phase delay');
        
        linkaxes([ax1, ax2, ax3], 'x');

      else
        
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             groupDelayWideband, ...
             'k-');
        hold on;
        semilogx(digitalAngFreq * obj.fs / (2 * pi), ...
             groupDelayWideband, ...
             'r--');
        hold off;
        xlim([10e-4, obj.fs / 2]);
        title('wideband group delay');
        ylabel('group delay (samples)');
        legend('group delay', 'phase delay');
        
      end
      xlabel('frequency (Hz)');
        
    end
    
  end
end