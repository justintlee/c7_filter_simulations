close all;
clear;
clc;

%-------------------------------------------------------------------------------
%SCRIPT PARAMETERS
%-------------------------------------------------------------------------------

f0 = 0.001;
f1 = 60;
tLength = 1e5;

flagCompress = 1;
flagFigure   = 0;

% compression parameters
wlevel  = 1;       % decomposition level
mywname = 'cdf97'; % wavelet family
PRD0    = 0.0;       % percent
quant   = 32;      % quantization level

%-------------------------------------------------------------------------------
%201.12.4.4.104
%
%The output at all possible GAIN settings shall be reproduced with a
%maximum amplitude error of +/- 10% compared to the test signal, referred
%to the input.
%
%Apply a 5 Hz, 2 mV p-v sinusoidal signal to all ECG input
%channels. The output shall comply with the above requirement at each
%possible GAIN setting.
%-------------------------------------------------------------------------------

% generate 5 Hz sinusoidal signal at 2 mV
fs = 250;
t = 0:(1/fs):tLength;

if flagCompress == 1
  s0F5p00 = floor(200 * sin(2 * pi * 5 * t));
else
  s0F5p00 = 2 * sin(2 * pi * 5 * t);
end

% run pre-filters
fc = FilterCascade(s0F5p00);
fc.multiplyByGain();
s1F5p00 = fc.applyFilter1();
s2F5p00 = fc.applyFilter2();

% run filter bank
[sHighF5p00, sLowF5p00] = fc.applyFilterBank();

% reconstruct the signal
sRF5p00 = reconstructsignal(sLowF5p00, sHighF5p00);

% compression
if flagCompress == 1
  % COMPRESS THE SIGNAL
  sR0F5p00 = sRF5p00;
  
  % compress the signal
  [vc, vi, vs, L] = compress(sRF5p00, wlevel, PRD0, quant);

  % decompress the signal
  [sRF5p00] = decompress(vc, vi, vs, L, wlevel, quant);
end

% compare amplitude values
[f, X0F5p00] = calcssb(s0F5p00, fs);
[~, XRF5p00] = calcssb(sRF5p00, fs);

indF5p00 = findclosestvalue(f, 5.00);
X0Amp5p00 = X0F5p00(indF5p00);              % |P0(5.00)|
XRAmp5p00 = XRF5p00(indF5p00);              % |PR(5.00)|

if flagCompress == 1
  [~, XR0F5p00] = calcssb(sR0F5p00, fs);
  XR0Amp5p00    = XR0F5p00(indF5p00);
end

absDiff = abs(X0Amp5p00 - XRAmp5p00);
refVal  = 0.10 * X0Amp5p00;
assert(absDiff <= refVal, '201.12.4.4.104 failed');

%-------------------------------------------------------------------------------
%201.12.4.4.108
%
%b.) The amplitude response to sinusoidal signals within the frequency range
%0.67 Hz to 40 Hz shall be between 140% and 70% (+3 dB to -3 dB) of the
%response at 5 Hz.
%
%If the manufacturer claims ST SEGMENT measurement capability for the ME
%EQUIPMENT, the lower cut-off frequency shall be 0.05 Hz for a first-order
%high-pass filter or its functional equivalent.
%
%If the manufacturer claims that the ME EQUIPMENT is capable of reocrding
%ECGs from infants weighing less than 10 kg, the upper cut-off frequency
%shall be at least 55 Hz.
%-------------------------------------------------------------------------------

% freqTestable    = [0.05, 0.5, 0.67, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, ...
%                    1.8, 1.9, 2.0, 7.0, 10.0, 15.0, 20.0, ...
%                    25.0, 30.0, 35.0, 40.0];
freqTestable    = [0.01:0.01:0.10 0.2:0.1:0.6 0.67 0.7:0.1:1.0 1.5:0.5:10 11:1:60];
numFreqTestable = length(freqTestable);
truthVal        = zeros(numFreqTestable, 1);
freqAmp         = zeros(numFreqTestable, 1);

if flagCompress == 1
  freqAmp0 = zeros(numFreqTestable, 1);
end

for k = 1:numFreqTestable
  disp(freqTestable(k));
  % generate new sinusoidal signal at 2 mV
  if flagCompress == 1
    s0 = floor(200 * sin(2 * pi * freqTestable(k) * t));
  else
    s0 = 2 * sin(2 * pi * freqTestable(k) * t);
  end

  % run pre-filters
  fc = FilterCascade(s0);
  fc.multiplyByGain();
  s1 = fc.applyFilter1();
  s2 = fc.applyFilter2();

  % run filter bank
  [sHigh, sLow] = fc.applyFilterBank();

  % reconstruct the signal
  sR = reconstructsignal(sLow, sHigh);
  
  % compression
  if flagCompress == 1
    sR0 = sR;
    
    % compress the signal
    [vc, vi, vs, L] = compress(sR, wlevel, PRD0, quant);
    
    % decompress the signal
    [sR] = decompress(vc, vi, vs, L, wlevel, quant);
  end

  % compare amplitude values
  [f, X0] = calcssb(s0, fs);
  [~, XR] = calcssb(sR, fs);

  ind = findclosestvalue(f, freqTestable(k));
  X0Amp = X0(ind);              % |P0(5.00)|
  XRAmp = XR(ind);              % |PR(5.00)|
  
  if flagCompress == 1
    [~, XR0]    = calcssb(sR0, fs);
    XR0Amp      = XR0(ind);
    freqAmp0(k) = XR0Amp;
  end

  freqAmp(k) = XRAmp;
  refVal     = XRAmp5p00;
  
  if (freqAmp(k) >= 0.7 * refVal) && (freqAmp(k) <= 1.4 * refVal)
    truthVal(k) = 1;
  end
  
end

ind0p05 = findclosestvalue(freqTestable, 0.05);
ind40p0 = findclosestvalue(freqTestable, 40.0);

expectedTruth = ones(length(ind0p05:ind40p0), 1);

assert(isequal(truthVal(ind0p05:ind40p0), ...
               expectedTruth), ...
       '201.12.4.4.108 failed');

disp('2-47 passed!');

if flagFigure == 1
  figure();
  plot(freqTestable, freqAmp);
  hold on;
  plot([freqTestable(1), freqTestable(end)], 0.7 * refVal * [1, 1], 'r--');
  plot([freqTestable(1), freqTestable(end)], 1.4 * refVal * [1, 1], 'r--');
  hold off;
  xlim([0, freqTestable(end)]);
  ylim([0, 1.4 * refVal + 10]);
  title('frequency response');
  xlabel('frequency (Hz)');
  ylabel('magnitude response');
end

%-------------------------------------------------------------------------------
%Plot 5 Hz data
%-------------------------------------------------------------------------------

if flagFigure == 1
  figure();
  plot(t, s0F5p00, 'k-');
  hold on;
  plot(t, sR0F5p00, 'r--');
  plot(t, sRF5p00, 'b--');
  hold off;
  xlim([7000, 7002]);
  xlabel('time (sec)');
  ylabel('amplitude');
  title('signal comparisons');
  legend('signal', 'reconstructed', 'decompressed');
end

%-------------------------------------------------------------------------------
%HELPER FUNCTIONS
%-------------------------------------------------------------------------------
function [f, X] = calcssb(x, fs)

% determine signal parameters
numSamples     = length(x);             % samples equal to double sideband
numHalfSamples = floor(numSamples / 2); % samples equal to single sideband

% generate frequency samples
f = fs * (0:numHalfSamples) / numSamples;

% run FFT
X = fft(x);
X = abs(X(1:numHalfSamples + 1) / numSamples);
X(2:end) = 2 * X(2:end);

end

function [signalReconstructed] = reconstructsignal(low, high)

signalReconstructed = (low + high / 4) / 75;

end

function [vc, vi, vs, L] = compress(s, wlevel, PRD0, quant)
    % COMPRESS THE SIGNAL
    
    % discrete CDF97 wavelet transform
    fw = waveletcdf97(s, wlevel);
    L = numel(fw);

    tol = PRD0 * norm(s) / 100;
    [cs, wind] = SLWC(fw, tol);

    % wavelet coeff. quantization
    csq = sign(cs) .* floor(abs(cs) / quant + 0.5);
    I0 = find(csq == 0);

    % remove the zeros
    if isempty(I0) == 0

      csq(I0)  = [];
      wind(I0) = [];

    end

    % organization and storage
    [or, ior] = sort(wind);
    vc = abs(csq(ior));

    % take differences
    vi = [or(1) diff(or)].'; 

    % encode the signs as zeros and ones
    vs = (sign(csq(ior)) + 1) ./ 2; 
end

function [sR] = decompress(vc, vi, vs, L, wlevel, quant)
    % DECOMPRESS THE SIGNAL
    orre = cumsum(vi);

    % recover signs
    vsre = 2 * vs - 1;
    cwre = zeros(L, 1);

    % recover nonzero coefficients
    cwre(orre) = vsre .* vc * quant;
    sD = waveletcdf97(cwre, -wlevel);

    % make the reconstruction signal the decompressed signal
    sR = sD;
end

function [ind] = findclosestvalue(f, testVal)
  % find the closest value
  [~, ind] = min(abs(bsxfun(@minus, f, testVal)));
end