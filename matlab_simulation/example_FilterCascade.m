close all; clear; clc;

fs = 250; % Hz
num_samples = 1e5;
n = 0:(num_samples - 1); % samples
t = n / fs;

n0 = 0:(10*num_samples - 1);
t0 = n0 / fs;

% generate chirp signal
% y = chirp(t, 1e-5, (num_samples - 1) / fs, 120, 'linear') + 10;
f0 = 0.0005; % Hz
f1 = 0.5;   % Hz
f2 = 5;     % Hz
f3 = 10;    % Hz
f4 = 20;    % Hz
f5 = 40;    % Hz
f6 = 50;    % Hz
f7 = 75;    % Hz
f8 = 100;   % Hz
y0 = cos(2 * pi * f0 * t0);
y1 = cos(2 * pi * f1 * t);
y2 = cos(2 * pi * f2 * t);
y3 = cos(2 * pi * f3 * t);
y4 = cos(2 * pi * f4 * t);
y5 = cos(2 * pi * f5 * t);
y6 = cos(2 * pi * f6 * t);
y7 = cos(2 * pi * f7 * t);
y8 = cos(2 * pi * f8 * t);
y = [y0 y1 y2 y3 y4 y5 y6 y7 y8];

t = (0:(length(y) - 1)) / fs;

% plot the chirp signal
figure();
num_subplots = 4;
ax1 = subplot(num_subplots, 2, 1);
subaxis_array = [ax1];
plot(t, y, 'k');
title('chirp function from 1e-4 Hz to 120 Hz');
xlim([t(1) t(end)]);
ylim([0.9 * min(y) 1.1 * max(y)]);
xlabel('sample #');
ylabel('amplitude');

% [py, fy] = calculate_energy_spectrum(y, fs);

ax2 = subplot(num_subplots, 2, 2);
% plot(fy, py, 'k');
% xlim([0, fs/2]);
spectrogram(y, 256, 250, 1024, fs, 'yaxis');
title('signal - FFT');
% xlabel('frequency (Hz)');
% ylabel('amplitude');

% check first stage filtering
test = FilterCascade(y);
test = test.multiplyByGain();
[~, test1] = test.applyFilter1();

ax3 = subplot(num_subplots, 2, 3);
subaxis_array = [subaxis_array; ax3;];
plot(t, test1.s, 'k');
title('results from first filter alone');
xlim([t(1) t(end)]);
ylim(1.1 * [min(test1.s) max(test1.s)]);
xlabel('sample #');
ylabel('amplitude');

[p1, f1] = calculate_energy_spectrum(test1.s, fs);

ax4 = subplot(num_subplots, 2, 4);
% plot(f1, p1, 'k');
% xlim([0, fs/2]);
title('results from first filter alone - FFT');
spectrogram(test1.s, 256, 250, 1024, fs, 'yaxis');
% xlabel('frequency (Hz)');
% ylabel('amplitude')

% check second stage filtering
[~, test2] = test.applyFilter2();

ax5 = subplot(num_subplots, 2, 5);
subaxis_array = [subaxis_array; ax5;];
plot(t, test2.s, 'k');
title('results from second filter alone');
xlim([t(1) t(end)]);
ylim(1.1 * [min(test2.s) max(test2.s)]);
xlabel('sample #');
ylabel('amplitude');

[p2, f2] = calculate_energy_spectrum(test2.s, fs);

ax6 = subplot(num_subplots, 2, 6);
% plot(f2, p2, 'k');
% xlim([0, fs/2]);
title('results from second filter alone - FFT');
spectrogram(test2.s, 256, 250, 1024, fs, 'yaxis');
% xlabel('frequency (Hz)');
% ylabel('amplitude')

% check cascaded first-second stage filtering
[~, test12] = test1.applyFilter2();

ax7 = subplot(num_subplots, 2, 7);
subaxis_array = [subaxis_array; ax7;];
plot(t, test12.s, 'k');
title('results from first and second filters cascaded');
xlim([t(1) t(end)]);
ylim(1.1 * [min(test12.s) max(test12.s)]);
xlabel('sample #');
ylabel('amplitude');

[p8, f8] = calculate_energy_spectrum(test12.s, fs);

ax8 = subplot(num_subplots, 2, 8);
% plot(f8, p8, 'k');
% xlim([0, fs/2]);
title('results from first and second filters cascaded - FFT');
spectrogram(test12.s, 256, 250, 1024, fs, 'yaxis');
% xlabel('frequency (Hz)');
% ylabel('amplitude')

linkaxes(subaxis_array, 'x');

%% FUNCTIONS

function [pxx, f] = calculate_energy_spectrum(x, fs)

  % create frequency axis
  fn = fs / 2;
  num_samples = length(x);
  f = (1:floor(num_samples / 2)) / num_samples * fs;
  
  % take the Fourier transform
  pxx = fft(x);
  pxx = pxx(1:floor(num_samples / 2));
  pxx = abs(pxx / length(pxx)).^2;
  pxx(2:end) = 2 * pxx(2:end);

end