close all; clear; clc;

fs = 250;

analogAngFreq = 2 * pi * logspace(-4, 3, 5e6); % rad / s
digitalAngFreq  = analogAngFreq / (fs / 2);    % rad / sample

R47 = 750e3;  % Ohms
R48 = 750e3;  % Ohms
R55 = 750e3;  % Ohms
R52 = 1500e3; % Ohms

C39 = 4.7e-9; % F
C40 = 4.7e-9; % F

% create gain (K = 1 + R4/R3)
K2 = 1 + R55 / R52;

% create quality factor Q
%           sqrt(R1R2C1C2)
% Q = -------------------------
%     R1C1 + R2C1 + R1C2(1 - K)
%     R1    R2    C1    C2
D21 = R48 * R47 * C39 * C40;
D22 = R48 * C39 ...
      + R47 * C39 ...
      + R48 * C40 * (1 - K2);
Q2  = sqrt(D21) / D22;

% create transfer function
analogFilter2 = struct();
analogFilter2.b = K2;
analogFilter2.a = [D21, D22, 1];

% determine analog frequency response
analogFreqResp = freqs(analogFilter2.b, ...
                       analogFilter2.a, ...
                       analogAngFreq);      % complex freq. resp.
analogFreqMag  = abs(analogFreqResp);       % magnitude

freqCutoff             = 1/(2 * pi * sqrt(D21)); % Hz
analogAngFreqCutoffVal = 2 * pi * freqCutoff;    % rad / s

% transform to digital filter
[b_digital, a_digital] = bilinear(analogFilter2.b, ...
                                  analogFilter2.a, ...
                                  fs, ...
                                  freqCutoff);
digitalFilter2 = dfilt.df1(b_digital, a_digital);

% determine digital frequency response
digitalFreqResp = digitalFilter2.freqz(digitalAngFreq); % complex freq. resp. 
digitalFreqMag  = abs(digitalFreqResp);                 % magnitude

% create prototype digital filter
[z, p, k] = buttap(2);
[num, den] = zp2tf(z, p, K2);
[num, den] = lp2lp(num, den, 2 * pi * freqCutoff);
protoFreqResp = freqs(num, ...
                      den, ...
                      analogAngFreq); % complex freq. resp.
protoMagResp  = abs(protoFreqResp);   % magnitude

figure();
plot(analogAngFreq / (2 * pi), analogFreqMag, 'k');
hold on;
plot(analogAngFreq / (2 * pi), protoMagResp, 'c--');
plot(digitalAngFreq * fs / (2 * pi), digitalFreqMag, 'b--');
plot(analogAngFreqCutoffVal / (2 * pi) * ones(1, 2), [0, 1.5], 'r--');
hold off;
xlim([1e-4, 125]);
legend('analog frequency response', ...
       'digital frequency response (bilinear)');
title('analog vs. digital filter response');
xlabel('frequency (Hz)');
ylabel('magnitude');
% ylabel('dB');